//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <meta/concepts.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace routine
    {
        template <class T>
        concept periodic_task_function_object = requires (T v) {
            typename T::duration_type;
            v(std::declval<typename T::duration_type>());
        };

        template <class T>
        struct task_traits {};

        template <periodic_task_function_object T>
        struct task_traits<T>
        {
            using value_type = T;
            using duration_type = typename T::duration_type;

            static constexpr bool equal(const value_type & value, const T & task) {
                return value == task;
            }

            static constexpr void execute(value_type & task, duration_type dt) {
                task(dt);
            }
        };

        template <periodic_task_function_object T>
        struct task_traits<T &>
        {
            using value_type = std::reference_wrapper<T>;
            using duration_type = typename T::duration_type;

            static constexpr bool equal(const value_type & value, const T & task) {
                return &value.get() == &task;
            }

            static constexpr void execute(value_type & task, duration_type dt) {
                task(dt);
            }
        };

        template <class T>
        concept periodic_task = requires (T v) {
            typename task_traits<T>::value_type;
            typename task_traits<T>::duration_type;
        };
    }
}
