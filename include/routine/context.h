//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <routine/executor.h>
#include <container/map.h>
#include <container/unique_ptr.h>

#include <ctti/type_id.hpp>

//---------------------------------------------------------------------------

namespace asd
{
    namespace routine
    {
        template <class Duration>
        class context
        {
        public:
            using allocator_type = std::pmr::polymorphic_allocator<context>;
            using duration_type = Duration;

            context(const allocator_type & alloc = {}) noexcept :
                _executors(alloc)
            {}

            context(context &&) = default;
            context & operator = (context &&) = default;

            template <routine::periodic_task T>
            routine::executor<T> & executor(meta::type<T> = {}) {
                using executor_type = std::conditional_t<
                    std::is_same_v<typename routine::executor<T>::duration_type, duration_type>,
                    routine::executor<T>,
                    routine::adapter<routine::executor<T>, duration_type>
                >;

                constexpr auto index = ctti::unnamed_type_id<executor_type>();

                auto it = _executors.find(index);

                if (it != _executors.end()) {
                    return static_cast<executor_type &>(*it->second);
                }

                return static_cast<executor_type &>(*_executors.emplace(index, pmr::make_unique<executor_type>(_executors.get_allocator())).first->second);
            }

            void operator()(duration_type dt) {
                for (auto & [_, executor] : _executors) {
                    (*executor)(dt);
                }
            }

        private:
            pmr::map<ctti::type_index, pmr::unique_ptr<routine::base_executor<duration_type>>> _executors;
        };
    }
}
