//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <routine/periodic_task.h>
#include <vector>
#include <chrono>

//---------------------------------------------------------------------------

namespace asd::routine
{
    template <class Duration>
    class base_executor
    {
    public:
        using duration_type = Duration;

        base_executor() = default;
        base_executor(const base_executor &) = delete;
        base_executor & operator = (const base_executor &) = delete;
        virtual ~base_executor() = default;

        virtual void operator()(duration_type dt) = 0;
    };

    template <class T>
    class executor : public base_executor<typename task_traits<T>::duration_type>
    {
    public:
        using allocator_type = std::pmr::polymorphic_allocator<executor>;
        using traits = task_traits<T>;
        using duration_type = typename traits::duration_type;

        executor(const allocator_type & alloc = {}) :
            _tasks(alloc)
        {}

        executor(std::allocator_arg_t, const allocator_type & alloc) :
            _tasks(alloc)
        {}

        void resume(T && a) {
            static_assert(routine::periodic_task<T>);
            _tasks.emplace_back(a);
        }

        void pause(T && a) {
            _tasks.erase(std::find_if(_tasks.begin(), _tasks.end(), [&](auto && value) {
                return traits::equal(a, value);
            }));
        }

        void operator()(duration_type dt) override final {
            for (auto && task : _tasks) {
                traits::execute(task, dt);
            }
        }

    private:
        std::pmr::vector<typename traits::value_type> _tasks;
    };

    template <class Executor, class Duration>
    class adapter : public base_executor<Duration>
    {
    public:
        using duration_type = Duration;

        template <class ... A>
        adapter(A && ... args) :
            _executor(std::forward<A>(args)...)
        {}

        void operator()(duration_type dt) override final {
            _executor(std::chrono::duration_cast<typename Executor::duration_type>(dt));
        }

        operator const Executor & () const {
            return _executor;
        }

        operator Executor & () {
            return _executor;
        }

    private:
        Executor _executor;
    };
}

namespace std
{
    template <class Executor, class Duration, class Allocator>
    struct uses_allocator<asd::routine::adapter<Executor, Duration>, Allocator> :
        uses_allocator<Executor, Allocator>
    {};
}

namespace asd::routine
{
    template <class T>
    class basic_periodic_task
    {
    public:
        using executor_type = routine::executor<T &>;

    protected:
        basic_periodic_task(executor_type & executor) noexcept :
            _executor(&executor)
        {}

        basic_periodic_task(basic_periodic_task && a) noexcept :
            _executor(std::exchange(a._executor, nullptr)),
            _active(std::exchange(a._active, false))
        {
            if (_executor && _active) {
                _executor->pause(static_cast<T &>(a));
                _executor->resume(static_cast<T &>(*this));
            }
        }

        ~basic_periodic_task() noexcept {
            if (_executor && _active) {
                _executor->pause(static_cast<T &>(*this));
            }
        }

        basic_periodic_task & operator = (basic_periodic_task && a) noexcept {
            _executor = std::exchange(a._executor, nullptr);
            _active = std::exchange(a._active, false);

            if (_executor && _active) {
                _executor->pause(static_cast<T &>(a));
                _executor->resume(static_cast<T &>(*this));
            }

            return *this;
        }

        bool active() const {
            return _active;
        }

        void set_active(bool active) {
            if (_active == active) {
                return;
            }

            _active = active;

            if (_active) {
                _executor->resume(static_cast<T &>(*this));
            } else {
                _executor->pause(static_cast<T &>(*this));
            }
        }

    private:
        executor_type * _executor;
        bool _active = false;
    };
}
