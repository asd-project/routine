import os

from conans import ConanFile, CMake

project_name = "routine_bench"


class RoutineBenchConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    author = "bright-composite"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "include*", "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.build_tools/0.0.1@asd/testing",
        "asd.math/0.0.1@asd/testing",
        "asd.routine/0.0.1@asd/testing",
        "function_ref/1.0.0@asd/testing",
        "benchmark/[>=1.5.0]"
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", src="include", dst="include")
        self.copy("*bench*", src="bin", dst="bin", keep_path=False)
