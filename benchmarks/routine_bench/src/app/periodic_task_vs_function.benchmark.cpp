#include <benchmark/benchmark.h>
#include <routine/context.h>

#include <container/function.h>
#include <tl/function_ref.hpp>

#include <math/point.h>

using namespace asd;
using namespace std::chrono_literals;
using common_duration_type = std::chrono::nanoseconds;

struct light_task
{
    using duration_type = common_duration_type;

    void operator()(duration_type dt) {
        t += dt;
    }

    duration_type t;
};

struct heavy_task
{
    using duration_type = common_duration_type;

    void operator()(duration_type dt) {
        t += dt;
        pos = {math::cos(t.count() / 1000.0f), math::sin(t.count() / 1000.0f)};
    }

    duration_type t;
    math::float_point pos;
};

static void periodic_task_create(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};

    for (auto _ : state) {
        for (size_t i = 0; i < state.range(0); ++i) {
            context.executor<light_task>().resume({});
            context.executor<heavy_task>().resume({});
        }

        benchmark::DoNotOptimize(context);
        benchmark::ClobberMemory();
    }
}

static void periodic_task_create_direct(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    auto & light_executor = context.executor<light_task>();
    auto & heavy_executor = context.executor<heavy_task>();

    for (auto _ : state) {
        for (size_t i = 0; i < state.range(0); ++i) {
            light_executor.resume({});
            heavy_executor.resume({});
        }

        benchmark::DoNotOptimize(context);
        benchmark::ClobberMemory();
    }
}

static void function_create(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    std::pmr::vector<asd::function<void(common_duration_type)>> tasks{&resource};

    for (auto _ : state) {
        for (size_t i = 0; i < state.range(0); ++i) {
            tasks.push_back(light_task{});
            tasks.push_back(heavy_task{});
        }

        benchmark::DoNotOptimize(tasks);
        benchmark::ClobberMemory();
    }
}

static void periodic_task_create_ref(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    std::pmr::vector<heavy_task> heavy_tasks{size_t(state.range(0)), &resource};

    for (auto _ : state) {
        for (auto & task : light_tasks) {
            context.executor<light_task &>().resume(task);
        }

        for (auto & task : heavy_tasks) {
            context.executor<heavy_task &>().resume(task);
        }

        benchmark::DoNotOptimize(context);
        benchmark::ClobberMemory();
    }
}

static void periodic_task_create_ref_direct(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    std::pmr::vector<heavy_task> heavy_tasks{size_t(state.range(0)), &resource};

    auto & light_executor = context.executor<light_task &>();
    auto & heavy_executor = context.executor<heavy_task &>();

    for (auto _ : state) {
        for (auto & task : light_tasks) {
            light_executor.resume(task);
        }

        for (auto & task : heavy_tasks) {
            heavy_executor.resume(task);
        }

        benchmark::DoNotOptimize(context);
        benchmark::ClobberMemory();
    }
}

static void function_create_ref(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    std::pmr::vector<tl::function_ref<void(common_duration_type)>> task_refs{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    std::pmr::vector<heavy_task> heavy_tasks{size_t(state.range(0)), &resource};

    for (auto _ : state) {
        benchmark::DoNotOptimize(task_refs.data());

        for (auto & task : light_tasks) {
            task_refs.push_back(task);
        }

        for (auto & task : heavy_tasks) {
            task_refs.push_back(task);
        }

        benchmark::ClobberMemory();
    }
}

static void periodic_task_update_light(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    auto & executor = context.executor<light_task>();

    for (size_t i = 0; i < state.range(0); ++i) {
        executor.resume(light_task{});
    }

    for (auto _ : state) {
        context(16ms);
    }
}

static void periodic_task_update_light_direct(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    auto & executor = context.executor<light_task>();

    for (size_t i = 0; i < state.range(0); ++i) {
        executor.resume(light_task{});
    }

    for (auto _ : state) {
        executor(16ms);
    }
}

static void function_update_light(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    std::pmr::vector<asd::function<void(common_duration_type)>> tasks{&resource};

    for (size_t i = 0; i < state.range(0); ++i) {
        tasks.push_back(light_task{});
    }

    for (auto _ : state) {
        for (auto && task : tasks) {
            task(16ms);
        }
    }
}

static void periodic_task_update(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    auto & light_executor = context.executor<light_task>();
    auto & heavy_executor = context.executor<heavy_task>();

    for (size_t i = 0; i < state.range(0); ++i) {
        light_executor.resume(light_task{});
        heavy_executor.resume(heavy_task{});
    }

    for (auto _ : state) {
        context(16ms);
    }
}

static void periodic_task_update_direct(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    auto & light_executor = context.executor<light_task>();
    auto & heavy_executor = context.executor<heavy_task>();

    for (size_t i = 0; i < state.range(0); ++i) {
        light_executor.resume(light_task{});
        heavy_executor.resume(heavy_task{});
    }

    for (auto _ : state) {
        light_executor(16ms);
        heavy_executor(16ms);
    }
}

static void function_update(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    std::pmr::vector<asd::function<void(common_duration_type)>> tasks{&resource};

    for (size_t i = 0; i < state.range(0); ++i) {
        tasks.push_back(light_task{});
        tasks.push_back(heavy_task{});
    }

    for (auto _ : state) {
        for (auto && task : tasks) {
            task(16ms);
        }
    }
}

static void periodic_task_update_light_ref(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    auto & executor = context.executor<light_task &>();

    for (auto & task : light_tasks) {
        executor.resume(task);
    }

    for (auto _ : state) {
        context(16ms);
    }
}

static void periodic_task_update_light_ref_direct(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    auto & executor = context.executor<light_task &>();

    for (auto & task : light_tasks) {
        executor.resume(task);
    }

    for (auto _ : state) {
        executor(16ms);
    }
}

static void function_update_light_ref(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    std::pmr::vector<tl::function_ref<void(common_duration_type)>> task_refs{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};

    for (auto & task : light_tasks) {
        task_refs.push_back(task);
    }

    for (auto _ : state) {
        for (auto && light_task : task_refs) {
            light_task(16ms);
        }
    }
}

static void periodic_task_update_ref(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    std::pmr::vector<heavy_task> heavy_tasks{size_t(state.range(0)), &resource};
    auto & light_executor = context.executor<light_task &>();
    auto & heavy_executor = context.executor<heavy_task &>();

    for (auto & task : light_tasks) {
        light_executor.resume(task);
    }

    for (auto & task : heavy_tasks) {
        heavy_executor.resume(task);
    }

    for (auto _ : state) {
        context(16ms);
    }
}

static void periodic_task_update_ref_direct(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    routine::context<common_duration_type> context{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    std::pmr::vector<heavy_task> heavy_tasks{size_t(state.range(0)), &resource};
    auto & light_executor = context.executor<light_task &>();
    auto & heavy_executor = context.executor<heavy_task &>();

    for (auto & task : light_tasks) {
        light_executor.resume(task);
    }

    for (auto & task : heavy_tasks) {
        heavy_executor.resume(task);
    }

    for (auto _ : state) {
        light_executor(16ms);
        heavy_executor(16ms);
    }
}

static void function_update_ref(benchmark::State& state) {
    std::pmr::monotonic_buffer_resource resource;
    std::pmr::vector<tl::function_ref<void(common_duration_type)>> task_refs{&resource};
    std::pmr::vector<light_task> light_tasks{size_t(state.range(0)), &resource};
    std::pmr::vector<heavy_task> heavy_tasks{size_t(state.range(0)), &resource};

    for (auto & task : light_tasks) {
        task_refs.push_back(task);
    }

    for (auto & task : heavy_tasks) {
        task_refs.push_back(task);
    }

    for (auto _ : state) {
        for (auto && task : task_refs) {
            task(16ms);
        }
    }
}

BENCHMARK(periodic_task_create)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_create_direct)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(function_create)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_create_ref)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_create_ref_direct)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(function_create_ref)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);

BENCHMARK(periodic_task_update_light)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_update_light_direct)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(function_update_light)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_update)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_update_direct)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(function_update)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);

BENCHMARK(periodic_task_update_light_ref)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_update_light_ref_direct)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(function_update_light_ref)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_update_ref)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(periodic_task_update_ref_direct)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);
BENCHMARK(function_update_ref)->Arg(8)->Arg(64)->Arg(512)->Arg(4096);

BENCHMARK_MAIN();
