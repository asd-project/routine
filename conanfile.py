import os

from conans import ConanFile, CMake

project_name = "routine"


class RoutineConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Utilities to simplify work with heterogeneous processes which are iterative and stateful (e.g. animations or progress visualizers)"
    topics = ("asd", project_name)
    generators = "cmake"
    exports_sources = "include*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.meta/0.0.1@asd/testing",
        "asd.container/0.0.1@asd/testing"
    )

    def source(self):
        pass

    def build(self):
        pass

    def package(self):
        self.copy("*.h", dst="include", src="include")
        self.copy("*.hpp", dst="include", src="include")
        self.copy("*.ipp", dst="include", src="include")

    def package_info(self):
        self.info.header_only()
